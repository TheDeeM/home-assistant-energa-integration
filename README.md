# Home Assistant Energa Integration

A simple Docker image that scraps the Energa Web Application (since there is no API yet) to gather the counter energy usage data

Keep in mind that I have prepared the image only for Raspberry Pi (arm devices) - help with preparing images for other
distributions is greatly welcomed!

## Usage

The first step to use the image is to register your account on the 
[MojLicznik Energa S.A](https://mojlicznik.energa-operator.pl/dp/UserData.do) website. You will need your
PESEL, PPE and meter number.

The image also integrates with the MQTT server, so you should install it beforehand as well.

```docker-compose
---
version: '3.3'
services:
  energa-meter-integration:
    image: thedeem/home-assistant-energa-integration:latest
    # Without proper access the image will not be able to start Selenide process and will crash
    # TODO: Try to find a way to remove the privileged requirement
    privileged: true
    environment:
      # Parameters used to connect into the MQTT server
      MQTT_SERVER: mqtt-broker
      MQTT_USERNAME: 'username'
      MQTT_PASSWORD: 'password'
      # The prefix of the Home Assistant MQTT autodiscovery (https://www.home-assistant.io/docs/mqtt/discovery/)
      MQTT_TOPIC_PREFIX: 'homeassistant'
      # Parameters used to connect into the Energa S.A. MojLicznik site
      ENERGA_USERNAME: 'username'
      ENERGA_PASSWORD: 'password'
      # The interval of gathering the data from the Energa S.A. MojLicznik site
      DATA_REFRESH_INTERVAL: 86400
```

If everything is configured correctly, you should be able to see a new device and entity in the 
[MQTT integration](https://www.home-assistant.io/integrations/mqtt/).

The image also integrates with the `Energy dashboard` inside the Home Assistant.

### Examples

Example sensor:

![Example sensor](images/example_sensor.png)

## Limitations

1. Unfortunately, Energa S.A. did not provide a proper API for the integration with the smart counter.
   The image will use **web scraping method**, which is considered a bad idea and generally can break
   very easily (since every change of the HTML on the website can cause issues). I will try to update
   the image to match any changes on the website, but any help is welcomed.
2. The MojLicznik website refreshes very rarely (approximately once a day, during the 00:00-05:00 hours).
   This means that generally speaking, the `DATA_REFRESH_INTERVAL` with small values makes no sense, since 
   the data will not be refreshed anyway.

## Building

You can manually build the image using the command:

```bash
docker build -t thedeem/home-assistant-energa-integration:${IMAGE_VERSION} -t thedeem/home-assistant-energa-integration:latest .
```