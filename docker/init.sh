#!/bin/bash -e

echo "Starting the Energa web scrapper..."

${ENERGA_HOME}/energa/main.py --refresh_interval "${DATA_REFRESH_INTERVAL:-86400}" \
   --mqtt_server "${MQTT_SERVER}" --mqtt_topic_prefix "${MQTT_TOPIC_PREFIX:-homeassistant}" --mqtt_username "${MQTT_USERNAME}" --mqtt_password "${MQTT_PASSWORD}" \
   --energa_username "${ENERGA_USERNAME}" --energa_password "${ENERGA_PASSWORD}";
