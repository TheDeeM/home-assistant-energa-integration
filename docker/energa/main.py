#!/usr/bin/env python3

from energa_data_gatherer import *
from energa_mqtt_integration import *
from helpers import Helpers
import logging
import time

Helpers.init_logging()

args = Helpers.get_program_arguments()
run_refresh_process = True
refresh_interval=args.refresh_interval

mqtt_integration = EnergaMqttHomeAssistantIntegration(args.mqtt_server, args.mqtt_username, args.mqtt_password, args.mqtt_topic_prefix)
energa_data_gatherer = EnergaDataGatherer(args.energa_username, args.energa_password)

logging.info('Starting the initial creation of the Home Assistant entities...')
energa_data = energa_data_gatherer.get_data_from_energa()
mqtt_integration.create_mqtt_entities(energa_data)

while (run_refresh_process):
    logging.info('Refreshing the Home Assistant entities with the new data...')
    try:
        energa_data = energa_data_gatherer.get_data_from_energa()
        mqtt_integration.update_mqtt_entities(energa_data)
    except EnergaDataGatheringError as error:
        logging.error('There was an error when getting a new data from the Energa site! Please check wheter the logging data is correct. Error: {error}'
            .format(error=error)
        )
    except MqttMessageSendingError as error:
        logging.error('There was an error when trying to update the MQTT server with the Energa data!. Error: {error}'
            .format(error=error)
        )
    except RuntimeError as error:
        logging.error('Got unknown error {error}... Stopping the process.'.format(error=error))
        run_refresh_process = False
    finally:
        logging.info('Waiting {time} seconds before the next update...'.format(time=refresh_interval))
        time.sleep(refresh_interval)
