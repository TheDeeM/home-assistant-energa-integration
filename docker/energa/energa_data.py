#!/usr/bin/env python3

class EnergaData():
    def __init__(self, attributes):
        self._energy_used = None
        self._energy_produced = None
        self._attributes = attributes

    def set_energy_used(self, energy_used):
        self._energy_used = energy_used

    def set_energy_produced(self, energy_produced):
        self._energy_produced = energy_produced

    def get_energy_used(self):
        return self._energy_used

    def get_energy_produced(self):
        return self._energy_produced

    def get_meter_number(self):
        return self._attributes['meter_number']

    def get_ppe_number(self):
        return self._attributes['ppe_number']

    def get_device_attribute(self, attribute_name):
        return self._attributes[attribute_name]
