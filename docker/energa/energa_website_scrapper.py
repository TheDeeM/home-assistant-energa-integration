#!/usr/bin/env python3

import re

class EnergaWebsiteScrapper():
    def __init__(self, driver):
        self._driver = driver

    def login(self, username, password):
        self._driver.find_element_by_id("loginRadio").click()
        self._driver.find_element_by_id("j_username").send_keys(username)
        self._driver.find_element_by_id ("j_password").send_keys(password)
        self._driver.find_element_by_name("loginNow").click()

    def get_used_energy(self):
        energy = self._driver.find_element_by_id("right").find_elements_by_tag_name("tr")[0].find_element_by_class_name("last").text
        return EnergaWebsiteScrapper.__process_energy_value(energy)

    def get_produced_energy(self):
        energy = self._driver.find_element_by_id("right").find_elements_by_tag_name("tr")[2].find_element_by_class_name("last").text
        return EnergaWebsiteScrapper.__process_energy_value(energy)

    def get_details_info(self):
        details = self._driver.find_element_by_class_name("detailsInfo").find_elements_by_tag_name("div")
        return EnergaWebsiteScrapper.__parse_details_info(details)

    def get_meter_number(self):
        meter_element = self._driver.find_elements_by_xpath('.//select[@id="meterSelectF"]/option[@selected]')[0].text
        return EnergaWebsiteScrapper.__extract_meter_number(meter_element)

    @staticmethod
    def __extract_meter_number(meter_info):
        return re.sub(r"(\d+).*", r"\1", meter_info)

    @staticmethod
    def __parse_details_info(details_info):
        details = {}
        for detail in details_info:
            full_text = detail.text
            label_element = detail.find_elements_by_tag_name("span")
            if label_element is not None and len(label_element) > 0:
                named_text = label_element[0].text
                value_text = full_text.replace(named_text, '').strip()
                proper_name = EnergaWebsiteScrapper.__get_detail_proper_name(named_text.strip())
                details[proper_name] = value_text
        details['contract_start_date'] = EnergaWebsiteScrapper.__parse_contract_start_date(details['contract_start_date'])
        return details

    @staticmethod
    # For example 'Od 2020-11-02'
    def __parse_contract_start_date(contract_field):
        return re.sub(r".*(\d{4}-\d{2}-\d{2})", r"\1", contract_field)

    @staticmethod
    def __get_detail_proper_name(detail_name):
        result = 'Unknown'
        if detail_name == 'Sprzedawca':
            result = 'seller'
        elif detail_name == 'Okres umowy':
            result = 'contract_start_date'
        elif detail_name == 'Numer PPE':
            result = 'ppe_number'
        elif detail_name == 'Taryfa':
            result = 'tariff'
        elif detail_name == 'Typ':
            result = 'contract_type'
        elif detail_name == 'Adres PPE':
            result = 'address'
        return result

    @staticmethod
    def __process_energy_value(data):
        data_fixed = data.replace(" ","").replace(",",".")
        return float(data_fixed)