#!/usr/bin/env python3

import argparse
import paho.mqtt.client as mqtt
import logging

class Helpers():
    @staticmethod
    def init_logging():
       logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

    @staticmethod
    def get_program_arguments():
        argsparser = argparse.ArgumentParser()
        argsparser.add_argument('-ms','--mqtt_server', help='The MQTT server address',required=True)
        argsparser.add_argument('-mu','--mqtt_username', help='The username used to login into the MQTT server',required=False)
        argsparser.add_argument('-mp','--mqtt_password', help='The password used to login into the MQTT server',required=False)
        argsparser.add_argument('-mt','--mqtt_topic_prefix', help='MQTT topic prefix. The default one for Home Assistant is "homeassistant".',
            required=True, default="homeassistant")
        argsparser.add_argument('-eu','--energa_username', help='Username used to login into the Energa site',required=True)
        argsparser.add_argument('-ep','--energa_password', help='Password to the Energa site',required=True)
        argsparser.add_argument('-ri','--refresh_interval', help='The refresh interval of the process in seconds', required=True, type=int)
        return argsparser.parse_args()
