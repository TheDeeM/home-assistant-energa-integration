#!/usr/bin/env python3

import logging
from enum import Enum
from energa_data import EnergaData
from energa_website_scrapper import EnergaWebsiteScrapper
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from pyvirtualdisplay import Display

class EnergaDataGatheringError(RuntimeError):
    pass

class EnergaDataGatherer():
     def __init__(self, energa_username, energa_password):
        self._energa_url = 'https://mojlicznik.energa-operator.pl/dp/UserLogin.do'
        self._energa_username = energa_username
        self._energa_password = energa_password

     def get_data_from_energa(self):
         logging.info("Downloading the data from Energa site " + self._energa_url)

         with Display(visible=False, size=(800, 600)) as display:
            driver = webdriver.Chrome('/usr/bin/chromedriver')
            driver.get(self._energa_url)
            scrapper = EnergaWebsiteScrapper(driver)

            logging.info('Logging to the Energa site with the provided credentials...')
            scrapper.login(self._energa_username, self._energa_password)
            logging.info('Connected correctly to the Energa site. Getting data...')

            try:
                used_energy = scrapper.get_used_energy()
                produced_energy = None
                details_info = scrapper.get_details_info()
                details_info['meter_number'] = scrapper.get_meter_number()
                try:
                    produced_energy = scrapper.get_produced_energy()
                    details_info['meter_type'] = 'bidirectional'
                except:
                    produced_energy = None
                    details_info['meter_type'] = 'unidirectional'
                logging.info('The meter {meter_number} is {meter_type}.'.format(
                    meter_number=details_info['meter_number'],
                    meter_type=details_info['meter_type']
                ))
            except RuntimeError as error:
                logging.error('Error when gathering data from the Energa site: {error}'.format(error=error))
                raise EnergaDataGatheringError
            finally:
               logging.info('Closing the connection to the Energa site...')
               driver.quit()

            logging.info('The data was correctly gathered from the Energa site!')
            energa_data = EnergaData(details_info)
            energa_data.set_energy_used(used_energy)
            energa_data.set_energy_produced(produced_energy)

            return energa_data
