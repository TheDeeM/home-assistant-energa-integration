#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import logging
import json

class MqttMessageSendingError(RuntimeError):
    pass

class EnergaMqttHomeAssistantIntegration():
    def __init__(self, mqtt_server, mqtt_user, mqtt_pass, mqtt_topic_prefix):
        self._mqtt_server = mqtt_server
        self._mqtt_user = mqtt_user
        self._mqtt_pass = mqtt_pass
        self._mqtt_topic_prefix = mqtt_topic_prefix

    def create_mqtt_entities(self, energa_data):
        device_info = EnergaMqttHomeAssistantIntegration.__get_device_info(energa_data)

        if energa_data.get_energy_used() is not None:
            sensor_id = EnergaMqttHomeAssistantIntegration.__get_sensor_id(energa_data, 'energy_used')
            config_topic = EnergaMqttHomeAssistantIntegration.__generate_config_topic(energa_data, sensor_id, self._mqtt_topic_prefix)
            state_topic = EnergaMqttHomeAssistantIntegration.__generate_state_topic(energa_data, self._mqtt_topic_prefix)
            attributes_topic = EnergaMqttHomeAssistantIntegration.__generate_attributes_change_topic(energa_data, self._mqtt_topic_prefix)
            config_payload = EnergaMqttHomeAssistantIntegration.__generate_config_payload(
                energa_data, device_info, state_topic, attributes_topic, sensor_id, 'Energy used', 'energy_used'
            )
            self.__send_mqtt_message(config_topic, json.dumps(config_payload))
            attributes_payload = EnergaMqttHomeAssistantIntegration.__generate_attributes_payload(energa_data)
            self.__send_mqtt_message(attributes_topic, json.dumps(attributes_payload))


    def update_mqtt_entities(self, energa_data):
        if energa_data.get_energy_used() is not None:
            sensor_id = EnergaMqttHomeAssistantIntegration.__get_sensor_id(energa_data, 'energy_used')
            state_topic = EnergaMqttHomeAssistantIntegration.__generate_state_topic(energa_data, self._mqtt_topic_prefix)
            state_payload = EnergaMqttHomeAssistantIntegration.__generate_state_payload(energa_data)
            self.__send_mqtt_message(state_topic, json.dumps(state_payload))

    @staticmethod
    def __get_sensor_id(energa_data, sensor_name):
        return 'energa_{meter_number}_energy_used'.format(meter_number=energa_data.get_meter_number())

    @staticmethod
    def __generate_config_topic(energa_data, sensor_id, mqtt_topic_prefix):
        return '{topic_prefix}/sensor/{device_id}/{sensor_id}/config'.format(
            topic_prefix=mqtt_topic_prefix,
            device_id=EnergaMqttHomeAssistantIntegration.__get_device_identifier(energa_data),
            sensor_id=sensor_id
        )

    @staticmethod
    def __generate_state_topic(energa_data, mqtt_topic_prefix):
        return '{topic_prefix}/sensor/{device_id}/state'.format(
           topic_prefix=mqtt_topic_prefix,
           device_id=EnergaMqttHomeAssistantIntegration.__get_device_identifier(energa_data)
       )

    @staticmethod
    def __generate_attributes_change_topic(energa_data, mqtt_topic_prefix):
        return '{topic_prefix}/sensor/{device_id}/attributes'.format(
           topic_prefix=mqtt_topic_prefix,
           device_id=EnergaMqttHomeAssistantIntegration.__get_device_identifier(energa_data)
       )

    @staticmethod
    def __get_device_identifier(energa_data):
        return 'energa_{meter_number}'.format(meter_number=energa_data.get_meter_number())

    @staticmethod
    def __get_device_info(energa_data):
        meter_number = energa_data.get_meter_number()
        ppe_number = energa_data.get_ppe_number()
        return {
            'identifiers': [EnergaMqttHomeAssistantIntegration.__get_device_identifier(energa_data)],
            'manufacturer': 'Energa S.A.',
            'model': 'Energa SmartLicznik',
            'name': 'Meter {meter_number} (PPE {ppe_number})'.format(meter_number=meter_number, ppe_number=ppe_number)
        }

    @staticmethod
    def __generate_config_payload(energa_data, device_info, state_topic, attributes_topic, sensor_id, sensor_name, sensor_value_name):
        payload = {
            'device': device_info,
            'device_class': 'energy',
            'state_class': 'total_increasing',
            'name': '{sensor_name} (Meter {meter_number})'.format(
                meter_number=energa_data.get_meter_number(),
                sensor_name=sensor_name
            ),
            'unit_of_measurement': 'kWh',
            'unique_id': sensor_id,
            'state_topic': state_topic,
            'value_template': '{{ value_json.' + sensor_value_name + ' }}',
            'json_attributes_topic': attributes_topic
        }
        return payload

    @staticmethod
    def __generate_state_payload(energa_data):
        payload = {
            'energy_used': energa_data.get_energy_used()
        }
        if energa_data.get_energy_produced() is not None:
            payload['energy_produced'] = energa_data.get_energy_produced()

        return payload

    @staticmethod
    def __generate_attributes_payload(energa_data):
        return {
            'seller': energa_data.get_device_attribute('seller'),
            'contract_start_date': energa_data.get_device_attribute('contract_start_date'),
            'ppe_number': energa_data.get_device_attribute('ppe_number'),
            'tariff': energa_data.get_device_attribute('tariff'),
            'contract_type': energa_data.get_device_attribute('contract_type'),
            'address': energa_data.get_device_attribute('address')
        }

    def __send_mqtt_message(self, mqtt_topic, mqtt_message):
        logging.info('Sending the message {mqtt_message} with topic "{topic}" to the MQTT server {server}...'.format(
            mqtt_message=mqtt_message, topic=mqtt_topic, server=self._mqtt_server
        ))

        def on_connect(client, userdata, flags, rc):
            client.publish(mqtt_topic, mqtt_message, 1, True)
            client.disconnect()

        client = mqtt.Client('energascript')

        if(self._mqtt_user != None):
            client.username_pw_set(self._mqtt_user, self._mqtt_pass)

        try:
            client.connect(self._mqtt_server)
        except RuntimeError as error:
            logging.error('Error when connecting to the MQTT server: {error}'.format(error=error))
            raise MqttMessageSendingError

        client.on_connect = on_connect
        client.loop_forever()
        return True
